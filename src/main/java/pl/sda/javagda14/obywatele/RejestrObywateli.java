package pl.sda.javagda14.obywatele;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class RejestrObywateli {
    private Map<String, Obywatel> mapaObywateli = new HashMap<>();

    public void dodajObywatela(String pesel, String imie, String nazwisko) {
        mapaObywateli.put(pesel, new Obywatel(pesel, imie, nazwisko));
    }

    public List<Obywatel> znajdzObywatelZRokuPrzed(int rok) {
        return mapaObywateli.values()           // wartości w mapie to obywatele
                .stream()                       // stream
                // filtracja po roku urodzenia
                .filter(obywatel -> rok > Integer.parseInt(obywatel.getPesel().substring(0, 2)))
                .collect(Collectors.toList());  // zebranie wyników do listy
    }

    public List<Obywatel> znajdzObywatelZRokuIZImieniem(int rok, String imie) {
        return mapaObywateli.values()           // wartości w mapie to obywatele
                .stream()                       // stream
                // filtracja po roku urodzenia
                .filter(obywatel -> rok == Integer.parseInt(obywatel.getPesel().substring(0, 2)))
                .filter(obywatel -> obywatel.getImie().equalsIgnoreCase(imie))
                .collect(Collectors.toList());  // zebranie wyników do listy
    }

    public List<Obywatel> znajdzObywatelaPoNazwisku(String nazwisko) {
        return mapaObywateli.values()           // wartości w mapie to obywatele
                .stream()                       // stream
                // filtracja po roku urodzenia
                .filter(obywatel -> obywatel.getNazwisko().equalsIgnoreCase(nazwisko))
                .collect(Collectors.toList());  // zebranie wyników do listy
    }

    public Optional<Obywatel> znajdzObywatelaPoPeselu(String pesel) {
        return Optional.ofNullable(mapaObywateli.get(pesel));
    }
}
