package pl.sda.javagda14.produkty;

public class Produkt {
    private String nazwa;
    private double cena;
    private ProductClass productClass;
    private ProductType type;

    public Produkt(String nazwa, double cena, ProductClass productClass, ProductType type) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.productClass = productClass;
        this.type = type;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public ProductClass getProductClass() {
        return productClass;
    }

    public void setProductClass(ProductClass productClass) {
        this.productClass = productClass;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }
}
