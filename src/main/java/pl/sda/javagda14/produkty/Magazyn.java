package pl.sda.javagda14.produkty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Magazyn {
    private Map<ProductClass, List<Produkt>> productClassListMap = new HashMap<>();
//    private Map<ProductType, List<Produkt>> productClassListMap = new HashMap<>();

    public void dodajProdukt(String nazwa, Double cena, ProductClass klas, ProductType type) {
        Produkt produkt = new Produkt(nazwa, cena, klas, type);

        List<Produkt> produktList = productClassListMap.get(klas);
        if (produktList == null) {
            produktList = new ArrayList<>();
        }
        produktList.add(produkt);

        productClassListMap.put(klas, produktList);
    }
}

