package pl.sda.javagda14.watki;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        Thread watek = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.err.println("" + i);
                }
            }
        });
        watek.start();

        for (int i = 0; i < 50; i++) {
            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("" + i);
        }

    }
}
