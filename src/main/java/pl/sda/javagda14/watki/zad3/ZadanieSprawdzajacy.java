package pl.sda.javagda14.watki.zad3;

import java.io.File;

public class ZadanieSprawdzajacy implements Runnable {
    private String sciezka;

    public ZadanieSprawdzajacy(String sciezka) {
        this.sciezka = sciezka;
    }


    @Override
    public void run() {
        try {

            int iloscPlikow = 0;
            while (true) {
                File folder = new File(sciezka);
                Thread.sleep(250);

                int pliki = folder.listFiles().length;

                if (pliki > iloscPlikow) {
                    System.out.println("Dodano plik");
                } else if (pliki < iloscPlikow) {
                    System.out.println("Usunięto plik");
                }
                iloscPlikow = pliki;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setSciezka(String sciezka) {
        this.sciezka = sciezka;
    }
}
