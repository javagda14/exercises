package pl.sda.javagda14.watki.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //////////////////////////////////////////
        MojeZadanie zadanie = new MojeZadanie();
        Thread watek = new Thread(zadanie);
        watek.start();

        ///////////////////////////////////////////
        Scanner scanner = new Scanner(System.in);
        String linia;
        do {
            // wczytuje linie
            linia = scanner.nextLine();

            // dodatek którego nie ma w treści
            if(linia.equalsIgnoreCase("int")){
                watek.interrupt();
                continue;
            }
            // przekazuje ją do zadania
            zadanie.setDodatkowyTekst(linia);
        } while (!linia.equalsIgnoreCase("quit"));
    }
}
