package pl.sda.javagda14.watki.zad2;

public class MojeZadanie implements Runnable {

    private String dodatkowyTekst;

    @Override
    public void run() {
        int i = 0;
        try {
            while (true) {
                System.out.println((i++) + ". Hello World " + dodatkowyTekst);
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setDodatkowyTekst(String dodatkowyTekst) {
        this.dodatkowyTekst = dodatkowyTekst;
    }
}
