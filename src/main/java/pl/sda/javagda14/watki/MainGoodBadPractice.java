package pl.sda.javagda14.watki;

public class MainGoodBadPractice {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int licznik = 0;
                    for (int i = 0; i < 10000000; i++) {
                        licznik++;
                        System.out.println(licznik);

                        Thread.sleep(1);
                    }
                    for (; licznik > 0; ) {
                        System.out.println(licznik--);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }
}
