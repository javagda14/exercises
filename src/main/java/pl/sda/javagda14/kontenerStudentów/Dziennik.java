package pl.sda.javagda14.kontenerStudentów;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dziennik {
    private Map<String, DaneStudenta> daneStudentow = new HashMap<>();
//    private Map<String, OcenyStudenta> ocenyStudenta = new HashMap<>();

    public void dodajStudenta(String nrIndeksu, String imie, String nazwisko) {
        daneStudentow.put(nrIndeksu, new DaneStudenta(imie, nazwisko, nrIndeksu));
    }

    public void dodajOceneDlaStudenta(String nrIndeksu, Przedmiot przedmiot, Integer ocena) {
        DaneStudenta daneStudenta = daneStudentow.get(nrIndeksu);

        if (daneStudenta != null) {
            daneStudenta.getOcenyStudenta().dodajOcene(przedmiot, ocena);
        } else {
            throw new IllegalArgumentException("Student z takim indeksem nie istnieje");
        }
    }

    public double podajSrednia(String indeks) {
        return daneStudentow.get(indeks).getOcenyStudenta().obliczSrednia();
    }

    public void getOceny(String indeks) {
        OcenyStudenta ocenyStudenta = daneStudentow.get(indeks).getOcenyStudenta();
        for (Map.Entry<Przedmiot, List<Integer>> ocenyZPrzedmiotu : ocenyStudenta.getOceny().entrySet()) {
            // klasa entry posiada metode getKey ( Przedmiot )
            // klasa entry posiada metode getValue ( List<Integer>)
            System.out.println("" + ocenyZPrzedmiotu.getKey() + " : " + ocenyZPrzedmiotu.getValue());
        }
    }
}
