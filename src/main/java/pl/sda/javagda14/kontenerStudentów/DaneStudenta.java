package pl.sda.javagda14.kontenerStudentów;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaneStudenta {
    private String imie, nazwisko, nrIndeksu;
    private OcenyStudenta ocenyStudenta = new OcenyStudenta();

    public DaneStudenta(String imie, String nazwisko, String nrIndeksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndeksu = nrIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(String nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public OcenyStudenta getOcenyStudenta() {
        return ocenyStudenta;
    }

    public void setOcenyStudenta(OcenyStudenta ocenyStudenta) {
        this.ocenyStudenta = ocenyStudenta;
    }
}
